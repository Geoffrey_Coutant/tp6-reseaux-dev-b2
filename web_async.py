import sys
import aiohttp, asyncio, aiofiles
from aiohttp import ClientSession

async def content(url: str, session: ClientSession) -> bytes:
    reponse = await session.get(url = url)
    if reponse.status != 200:
        print(f"GET {url}: {reponse.status}")
    return await reponse.content.read()

async def write_to_file(data: bytes, file: str):
    async with aiofiles.open(file, mode='wb') as file:
        await file.write(data)

async def lancer():
    async with aiohttp.ClientSession() as session:
        await write_to_file(await content(url, session), '/tmp/web_page')

url = sys.argv[1]
asyncio.run(lancer())
