import sys

import requests

def content(url: str) -> bytes:
    reponse = requests.get(url = url)
    if reponse.status_code != 200:
        print(f"GET {url}: {reponse.status_code}")
    return reponse.content

def write_to_file(file: str, data: bytes):
    write = open(file=file, mode='wb')
    write.write(data)
    write.close()

if __name__ == '__main__':
    url = sys.argv[1]

    write_to_file(content(url), 'web_page.html')
