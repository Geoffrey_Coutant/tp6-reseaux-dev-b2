import asyncio

async def count_to_ten():
    for i in range(1, 11):
        print(i)
        await asyncio.sleep(0.5)

loop = asyncio.get_event_loop()

loop.run_until_complete(asyncio.gather(count_to_ten(), count_to_ten()))

loop.close()